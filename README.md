# countdown
![countdown_screenshot](public/screenshot.png)

## Tutorial
``
https://www.youtube.com/watch?v=Q_fLx2KcoYA
``

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Run project local
```
npm run serve
```